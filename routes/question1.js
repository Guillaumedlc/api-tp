const express = require('express');
// Lodash utils library
const _ = require('lodash');
const axios = require('axios');

const router = express.Router();
const API_URL='http://www.omdbapi.com/';
const API_KEY='c4c9c962';

// Create RAW data array
let movies = [{
  id : String,
  movie: String,
  yearOfRelease: Number,
  duration: Number, //en minutes
  actors: [String, String],
  poster: String, //lien vers image 
  boxOffice: Number, //en USD$
  rottenTomatoesScore: Number 
}];

function movies(movie,yearOfRelease,duration,actors,poster,boxOffice,rottenTomatoesScore){
  this.movie = movie;
  this.yearOfRelease = yearOfRelease;
  this.duration = duration;
  this.actors = actors;
  this.poster = poster;
  this.boxOffice = boxOffice;
  this.rottenTomatoesScore = rottenTomatoesScore;
}
function movies(){
}

/* GET movies listing.*/
router.get('/omdb/:titre', (req, res) => {
  const{titre}=req.params;
  var m = new movies();
  axios.get(`${API_URL}?t=${titre}&apikey=${API_KEY}`)
  .then(function(response){
        console.log(response.data);
        res.json({data:response.data});
        m.movie = response.data.titre;
        m.yearOfRelease = response.data.duration;
        m.actors = response.data.actors;
        m.poster = response.data.poster;
        m.boxOffice = response.data.boxOffice;
        m.rottenTomatoesScore = response.data.ratings[1].value;
        console.log(m.movie);
   })
   .catch(function(error){
       console.log(error.data);
       res.json({message:'Erreur'});
   });
});
  /* GET one movie. */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    // Find movie in DB
    const movie = _.find(movies, ["id", id]);
    // Return movie
    res.status(200).json({
      message: 'Movie found!',
      movie
    });
    
});

  /* GET all movies. */
  router.get('', (req, res) => {
    const { id } = req.params;
    // Find movie in DB
    const movie = _.find(movies);
    // Return movie
    res.status(200).json({
      message: 'Any Movies found!',
      movie
    });
    
});

  /* PUT new movie. */
  router.put('/', (req, res) => {
    // Get the data from request from request
    const { movie } = req.body;
    // Create new unique id
    const id = _.uniqueId();
    // Insert it in array (normaly with connect the data with the database)
    movies.push({ movie, id });
    // Return message
    res.json({
      message: `Just added ${id}`,
      movie: { movie, id }
    });
  });
  
  /* DELETE movie. */
  router.delete('/:id', (req, res) => {
    // Get the :id of the movie we want to delete from the params of the request
    const { id } = req.params;
  
    // Remove from "DB"
    _.remove(movies, ["id", id]);
  
    // Return message
    res.json({
      message: `Just removed ${id}`
    });
  });
  
  /* UPDATE movie. */
  router.post('/:id', (req, res) => {
    // Get the :id of the movie we want to update from the params of the request
    const { id } = req.params;
    // Get the new data of the movie we want to update from the body of the request
    const { movie } = req.body;
    // Find in DB
    const movieToUpdate = _.find(movies, ["id", id]);
    // Update data with new data (js is by address)
    movieToUpdate.movie = movie;
  
    // Return message
    res.json({
      message: `Just updated ${id} with ${movie}`
    });
  });
  
  module.exports = router;
  
